import { CommonService } from './common.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private commonService: CommonService,
    private router: Router) { }


  canActivate(): boolean {
    console.log(this.commonService.loggedIn());

    if (this.commonService.loggedIn()) { return true }
    else {
      this.router.navigate(['/signin'])
      return false
    }
  }

}
