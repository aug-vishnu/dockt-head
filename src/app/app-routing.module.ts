import { CreditDashboardComponent } from './credit/credit-dashboard/credit-dashboard.component';
import { ReportManagementComponent } from './dockt/report-management/report-management.component';
import { InvoiceDetailComponent } from './dockt/invoice-detail/invoice-detail.component';
import { ProductManagementComponent } from './dockt/product-management/product-management.component';
import { PurchaseManagementComponent } from './dockt/purchase-management/purchase-management.component';
import { ExpenseManagementComponent } from './dockt/expense-management/expense-management.component';
import { SalesManagementComponent } from './dockt/sales-management/sales-management.component';
import { JournalManagementComponent } from './eegai/journal-management/journal-management.component';
import { VendorManagementComponent } from './eegai/vendor-management/vendor-management.component';
import { ManagerManagementComponent } from './eegai/manager-management/manager-management.component';
import { OwnerManagementComponent } from './eegai/owner-management/owner-management.component';
import { LeadManagementComponent } from './eegai/lead-management/lead-management.component';
import { TaskManagementComponent } from './eegai/task-management/task-management.component';
import { DashboardHomeComponent } from './dashboard/dashboard-home/dashboard-home.component';
import { AuthGuard } from './backend/auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { DashboardNavComponent } from './dashboard/dashboard-nav/dashboard-nav.component';
import { ShopProfileComponent } from './eegai/shop-profile/shop-profile.component';
import { InvoiceManagementComponent } from './eegai/invoice-management/invoice-management.component';
import { CustomerManagementComponent } from './eegai/customer-management/customer-management.component';
import { CustomerDetailComponent } from './eegai/customer-detail/customer-detail.component';


const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full', },

  // Auth
  { path: 'signin', component: LoginComponent },
  // { path:'signup', component: RegisterComponent },

  // Invoice Detail
  { path: 'invoice-detail', component: InvoiceDetailComponent },

  // Admin-Panel
  {
    path: 'dashboard',
    component: DashboardNavComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', component: DashboardHomeComponent },
      // { path: '', component: ArambHomeComponent },

      // Credit Dashboard
      { path: 'credit-management', component: CreditDashboardComponent },

      // Master DataBases
      { path: 'customer-management', component: CustomerManagementComponent },
      { path: 'customer-detail', component: CustomerDetailComponent },
      { path: 'vendor-management', component: VendorManagementComponent },
      { path: 'product-management', component: ProductManagementComponent },

      // Follow Ups
      { path: 'lead-management', component: LeadManagementComponent },
      { path: 'task-management', component: TaskManagementComponent },
      { path: 'invoice-management', component: InvoiceManagementComponent },

      // Journal
      { path: 'journal-management', component: JournalManagementComponent },
      { path: 'report-management', component: ReportManagementComponent },

      // Users
      { path: 'owner-management', component: OwnerManagementComponent },
      { path: 'manager-management', component: ManagerManagementComponent },

      // Shop Management
      { path: 'shop-management', component: ShopProfileComponent },

      // Dockt Sales - Expense - Purchase
      { path: 'sales-management', component: SalesManagementComponent },
      { path: 'expense-management', component: ExpenseManagementComponent },
      { path: 'purchase-management', component: PurchaseManagementComponent },



    ]
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
