import { NgForm } from '@angular/forms';
import { ShopService } from './../../backend/shop.service';
import { Router } from '@angular/router';
import { MatAccordion } from '@angular/material/expansion';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { Component, OnInit, ViewChild } from '@angular/core';

declare var $: any;
import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;

@Component({
  selector: 'app-purchase-management',
  templateUrl: './purchase-management.component.html',
  styleUrls: ['./purchase-management.component.scss'],
})
export class PurchaseManagementComponent implements OnInit {
  @ViewChild(MatAccordion) accordion: MatAccordion;

  constructor(private router: Router, private shopService: ShopService) {}
  ngOnInit(): void {
    /////////////////////////////////////////////////////////////////////////////////////////////
    this.list_factor();
    /////////////////////////////////////////////////////////////////////////////////////////////
    this.Factor.purchase_status = 2;
    this.Factor.purchase_payment = 2;
    this.Factor.account_type = 1;
  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Variables
  Factor: any = new Object();

  Factors: any = [];
  Vendors: any = [];
  Products: any = [];
  PURCHASE_PAYMENT: any = [
    { id: 1, value: 'Online' },
    { id: 2, value: 'Cash' },
    { id: 3, value: 'Credit' },
  ];
  PURCHASE_STATUS: any = [
    { id: 1, value: 'Partial Paid' },
    { id: 2, value: 'Fully Paid' },
  ];
  ACCOUNT_TYPE: any = [
    { id: 1, value: 'Account 1' },
    { id: 2, value: 'Account 2' },
  ];
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Expense Add
  create_factor(form: NgForm) {
    console.log(this.Factor);
    if (this.Factor.purchase_status == 2) {
      this.Factor.amount_paid = this.Factor.purchase_price;
    }
    this.shopService.purchase_create(this.Factor).subscribe((resp) => {
      form.reset();
      console.log(resp);
      this.list_factor();
      this.Factor = new Object();
      this.Factor.purchase_status = 2;
      this.Factor.purchase_payment = 2;
      this.Factor.account_type = 1;
      $('#create-factor-modal').modal('hide');
    });
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching expense List // expense
  list_factor() {
    this.Factor.filter_type = 1;
    this.shopService.purchase_list(this.Factor).subscribe((resp) => {
      this.Factors = resp;
      console.log(resp);
      this.rerender();
      $('.preloader').fadeOut();
      this.list_vendor();
    });
  }
  list_vendor() {
    this.shopService.vendor_list().subscribe((resp) => {
      this.Vendors = resp;
      console.log(resp);
    });
    this.list_product();
  }
  list_product() {
    this.shopService.product_list().subscribe((resp) => {
      this.Products = resp;
      console.log(resp);
    });
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // To Edit/Delete expense
  edit_factor(form: NgForm) {
    console.log(this.Factor);
    this.shopService.purchase_edit(this.Factor).subscribe((resp) => {
      form.reset();
      console.log('this.Factor');
      console.log(resp);
      this.list_factor();
      this.Factor = new Object();
      this.Factor.purchase_status = 2;
      this.Factor.purchase_payment = 2;
      $('#edit-factor-modal').modal('hide');
    });
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  delete_factor(element: any) {
    this.Factor = element;
    this.shopService.purchase_delete(this.Factor).subscribe((resp) => {
      this.list_factor();
    });
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Misc Functions
  show_edit(ele: any) {
    console.log(ele);
    // this.selectedFrom = ele.customer_from
    this.Factor = ele;
    console.log(ele);
    $('#edit-factor-modal').modal('show');
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  isExpand: any = false;
  @ViewChild(DataTableDirective, { static: false })
  dtEle: DataTableDirective;
  dtTrigger: Subject<any> = new Subject<any>();
  dtOptions: DataTables.Settings = {
    // responsive: true,
    // ordering: false
  };

  // DataTables Functions
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtEle.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }
  togglePanels() {
    if (this.isExpand) {
      this.accordion.closeAll();
    } else {
      this.accordion.openAll();
    }
    this.isExpand = !this.isExpand;
  }

  isTable = true;
  toggleView(view) {
    var text = view.target.textContent;
    if (text == 'Table View') {
      console.log(view.target.textContent);
      this.config['isTable'].product_database = true;
      Storage.set({ key: 'config', value: JSON.stringify(this.config) });
      this.list_factor();
    } else if (text == 'Panel View') {
      this.config['isTable'].product_database = false;
      Storage.set({ key: 'config', value: JSON.stringify(this.config) });
    }
  }

  config = this.shopService.config;
  update_config() {
    Storage.set({ key: 'config', value: JSON.stringify(this.config) });
  }

  /////////////////////////////////////////////////////////////////////////////////////////////
}
