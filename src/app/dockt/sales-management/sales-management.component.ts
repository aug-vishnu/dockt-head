import { NgForm } from '@angular/forms';
import { ShopService } from './../../backend/shop.service';
import { Router } from '@angular/router';
import { MatAccordion } from '@angular/material/expansion';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { Component, OnInit, ViewChild } from '@angular/core';

declare var $: any;
import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;

@Component({
  selector: 'app-sales-management',
  templateUrl: './sales-management.component.html',
  styleUrls: ['./sales-management.component.scss'],
})
export class SalesManagementComponent implements OnInit {
  @ViewChild(MatAccordion) accordion: MatAccordion;

  constructor(private router: Router, private shopService: ShopService) {}
  ngOnInit(): void {
    /////////////////////////////////////////////////////////////////////////////////////////////
    this.list_factor();
    /////////////////////////////////////////////////////////////////////////////////////////////
  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Variables
  Factor: any = new Object();
  Factors: any = [];
  Vendors: any = [];
  Products: any = [];
  PURCHASE_PAYMENT: any = [
    { id: 1, value: 'Online' },
    { id: 2, value: 'Cash' },
    { id: 3, value: 'Credit' },
  ];
  PURCHASE_STATUS: any = [
    { id: 1, value: 'Partial Paid' },
    { id: 2, value: 'Fully Paid' },
  ];
  ACCOUNT_TYPE: any = [
    { id: 1, value: 'Account 1' },
    { id: 2, value: 'Account 2' },
  ];

  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Expense Add
  create_factor(form: NgForm) {
    console.log(this.Factor);
    this.Factor.expense_payment = 1;
    this.shopService.sales_create(this.Factor).subscribe((resp) => {
      console.log(resp);
      this.list_factor();
      this.Factor = new Object();
      $('#create-factor-modal').modal('hide');
      form.reset();
    });
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching expense List // expense
  list_factor() {
    this.Factor.filter_type = 1;
    this.shopService.sales_list(this.Factor).subscribe((resp) => {
      this.Factors = resp;
      console.log(resp);
      this.rerender();
      $('.preloader').fadeOut();
      this.list_vendor();
    });
  }
  list_vendor() {
    this.shopService.vendor_list().subscribe((resp) => {
      this.Vendors = resp;
      console.log(resp);
    });
  }
  list_product() {
    this.shopService.product_list().subscribe((resp) => {
      this.Vendors = resp;
      console.log(resp);
    });
  }

  print_bill(factor) {
    this.router.navigate(['invoice-detail'], {
      queryParams: { invoice: factor.order_id },
    });
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // To Edit/Delete expense
  edit_factor(form: NgForm) {
    console.log(this.Factor);
    this.shopService.sales_edit(this.Factor).subscribe((resp) => {
      console.log('this.Factor');
      console.log(resp);
      this.list_factor();
      this.Factor = new Object();
      $('#edit-factor-modal').modal('hide');
      form.reset();
    });
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // To Edit/Delete expense
  edit_dispatch(form: NgForm) {
    console.log(this.Factor);
    this.shopService.dispatch_edit(this.Factor).subscribe((resp) => {
      console.log('this.Factor');
      console.log(resp);
      this.list_factor();
      this.Factor = new Object();
      $('#edit-dispatch-modal').modal('hide');
      form.reset();
    });
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  delete_factor(element: any) {
    this.Factor.order_id = element.order_id;
    console.log(this.Factor);
    this.shopService.sales_delete(this.Factor).subscribe((resp) => {
      console.log(resp);
      console.log(resp);
      this.list_factor();
    });
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Misc Functions
  show_edit(ele: any) {
    console.log(ele);
    // this.selectedFrom = ele.customer_from
    this.Factor = ele;
    console.log(ele);
    $('#edit-factor-modal').modal('show');
  }
  show_dispatch_edit(ele: any) {
    console.log(ele);
    // this.selectedFrom = ele.customer_from
    this.Factor = ele;
    console.log(ele);
    $('#edit-dispatch-modal').modal('show');
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  isExpand: any = false;
  @ViewChild(DataTableDirective, { static: false })
  dtEle: DataTableDirective;
  dtTrigger: Subject<any> = new Subject<any>();
  dtOptions: DataTables.Settings = {
    // responsive: true,
    // ordering: false
  };

  // DataTables Functions
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtEle.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }
  togglePanels() {
    if (this.isExpand) {
      this.accordion.closeAll();
    } else {
      this.accordion.openAll();
    }
    this.isExpand = !this.isExpand;
  }
  isTable = true;
  toggleView(view) {
    var text = view.target.textContent;
    if (text == 'Table View') {
      console.log(view.target.textContent);
      this.config['isTable'].product_database = true;
      Storage.set({ key: 'config', value: JSON.stringify(this.config) });
      this.list_factor();
    } else if (text == 'Panel View') {
      this.config['isTable'].product_database = false;
      Storage.set({ key: 'config', value: JSON.stringify(this.config) });
    }
  }

  config = this.shopService.config;
  update_config() {
    Storage.set({ key: 'config', value: JSON.stringify(this.config) });
  }

  /////////////////////////////////////////////////////////////////////////////////////////////
}
