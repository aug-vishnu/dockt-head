import { MatAccordion } from '@angular/material/expansion';
import { ShopService } from './../../backend/shop.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { SiteService } from './../../backend/site.service';
import { Subject } from 'rxjs';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
declare var $: any;

import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;
@Component({
  selector: 'app-product-management',
  templateUrl: './product-management.component.html',
  styleUrls: ['./product-management.component.scss']
})
export class ProductManagementComponent implements OnInit {


  @ViewChild(MatAccordion) accordion: MatAccordion;

  constructor(private router: Router, private shopService: ShopService) { }
  ngOnInit(): void {

    /////////////////////////////////////////////////////////////////////////////////////////////
    this.list_factor()
    this.Factor.product_template = 1
    /////////////////////////////////////////////////////////////////////////////////////////////

  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Variables
  active_day = new Date()

  Factor: any = new Object();
  Factors: any = []; Vendors: any = []; Products: any = [];
  PRODUCT_TYPE: any = [{ id: 1, value: 'Gram' }, { id: 2, value: 'Kilo Gram' }, { id: 3, value: 'Piece' }, { id: 4, value: 'Dozens' }, { id: 5, value: 'Litre' }, { id: 6, value: 'Milli Litre' }, { id: 7, value: 'Cases' }]
  PRODUCT_TEMPLATES: any = [{ id: 1, value: 'Product - Non-Expiry' }, { id: 2, value: 'Product - Expiry' }, { id: 3, value: 'Service' }]
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching expense List // expense
  list_factor() {
    this.shopService.product_list().subscribe(
      resp => {
        this.Factors = resp
        console.log(resp);
        this.rerender()
        $(".preloader").fadeOut();
      })
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Expense Add
  create_factor(form: NgForm) {
    console.log(this.Factor);
    if (this.Factor.product_template == 3) {
      this.Factor.product_expiry = null
      this.Factor.product_stock = 0
      this.Factor.product_gst = 0
      this.Factor.product_value = null
      this.Factor.product_type = null
    }
    if (this.Factor.product_template == 1) {
      this.Factor.product_expiry = null
    }
    // this.Factor.expense_payment = 1
    this.shopService.product_create(this.Factor).subscribe(
      resp => {
        console.log(resp)
        this.list_factor()
        $('#create-factor-modal').modal('hide');
        form.reset()
        this.Factor = new Object()
        this.active_day = new Date()
        this.Factor.product_template = 1

      },
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // To Edit/Delete expense
  edit_factor(form: NgForm) {
    console.log(this.Factor);
    if (this.Factor.product_template == 3) {
      this.Factor.product_expiry = null
      this.Factor.product_stock = null
      this.Factor.product_gst = null
      this.Factor.product_value = null
      this.Factor.product_type = null
    }
    if (this.Factor.product_template == 1) {
      this.Factor.product_expiry = null
    }
    this.shopService.product_edit(this.Factor).subscribe(
      resp => {
        console.log("this.Factor");
        console.log(resp)
        this.list_factor()
        this.Factor = new Object()
        $('#edit-factor-modal').modal('hide');
        form.reset()
        this.Factor.product_template = 1

      },
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  delete_factor(element: any) {
    this.Factor = element
    this.shopService.product_delete(this.Factor).subscribe(
      resp => {
        $('#edit-factor-modal').modal('hide');
        this.list_factor()
      },
    )
  }
  /////////////////////////////////////////////////////////////////////////////////////////////





  /////////////////////////////////////////////////////////////////////////////////////////////
  // Timer Functions
  // Input : Raw DateTime // Output : dd-mm-yy
  DateTimetoDay(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('-');
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Misc Functions
  onDateChange(e) {
    console.log(this.active_day);
    this.active_day = e.target.value
    this.list_factor()
  }

  show_edit(ele: any) {
    console.log(ele);
    // this.selectedFrom = ele.customer_from
    this.Factor = ele
    console.log(ele);
    $('#edit-factor-modal').modal('show')
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  isExpand: any = false
  @ViewChild(DataTableDirective, { static: false })
  dtEle: DataTableDirective;
  dtTrigger: Subject<any> = new Subject<any>();
  dtOptions: DataTables.Settings = {
    pageLength: 100,

    // responsive: true,
    // ordering: false
  };

  // DataTables Functions
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtEle.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }
  togglePanels() {
    if (this.isExpand) { this.accordion.closeAll() }
    else { this.accordion.openAll() }
    this.isExpand = !this.isExpand
  }

  isTable = true
  toggleView(view) {
    var text = view.target.textContent
    if (text == "Table View") {
      console.log(view.target.textContent);
      this.config['isTable'].product_database = true
      Storage.set({ key: 'config', value: JSON.stringify(this.config) });
      this.list_factor()
    }
    else if (text == "Panel View") {
      this.config['isTable'].product_database = false
      Storage.set({ key: 'config', value: JSON.stringify(this.config) });
    }
  }

  config = this.shopService.config
  update_config() {
    Storage.set({ key: 'config', value: JSON.stringify(this.config) });
  }

  /////////////////////////////////////////////////////////////////////////////////////////////



}
