import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DesignIn002Component } from './design-in002.component';

describe('DesignIn002Component', () => {
  let component: DesignIn002Component;
  let fixture: ComponentFixture<DesignIn002Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DesignIn002Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DesignIn002Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
