import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DesignIn005Component } from './design-in005.component';

describe('DesignIn005Component', () => {
  let component: DesignIn005Component;
  let fixture: ComponentFixture<DesignIn005Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DesignIn005Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DesignIn005Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
