import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesignInd001Component } from './design-ind001.component';

describe('DesignInd001Component', () => {
    let component: DesignInd001Component;
    let fixture: ComponentFixture<DesignInd001Component>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [DesignInd001Component]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DesignInd001Component);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should be created', () => {
        expect(component).toBeTruthy();
    });
});
