import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-design-ind001',
  templateUrl: './design-ind001.component.html',
  styleUrls: ['./design-ind001.component.scss']
})
export class DesignInd001Component {
  @Input() Invoice
  constructor() { }
}
