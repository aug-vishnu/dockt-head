import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DesignIn003Component } from './design-in003.component';

describe('DesignIn003Component', () => {
  let component: DesignIn003Component;
  let fixture: ComponentFixture<DesignIn003Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DesignIn003Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DesignIn003Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
