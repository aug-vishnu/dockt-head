import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DesignIn004Component } from './design-in004.component';

describe('DesignIn004Component', () => {
  let component: DesignIn004Component;
  let fixture: ComponentFixture<DesignIn004Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DesignIn004Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DesignIn004Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
