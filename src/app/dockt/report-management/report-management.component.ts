import { ShopService } from './../../backend/shop.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-report-management',
  templateUrl: './report-management.component.html',
  styleUrls: ['./report-management.component.scss']
})
export class ReportManagementComponent implements OnInit {

  constructor(private router: Router, private shopService: ShopService) { }

  ngOnInit(): void {
    this.fetch_report()
  }

  Report: any = new Object()
  fetch_report() {
    this.Report.filter_type = 1
    this.shopService.report_detail(this.Report).subscribe(
      resp => {
        console.log(resp)
        $(".preloader").fadeOut();

        this.Report = resp
      },
    );
  }

}
