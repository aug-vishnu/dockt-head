import { ShopService } from './../../backend/shop.service';
import { Component, OnInit, Pipe, PipeTransform, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

declare var $: any;
import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;


@Component({
  selector: 'app-invoice-detail',
  templateUrl: './invoice-detail.component.html',
  styleUrls: ['./invoice-detail.component.scss']
})
export class InvoiceDetailComponent implements OnInit {
  invoice_id;
  constructor(private activatedRoute: ActivatedRoute, private shopService: ShopService) {
    this.activatedRoute.queryParams.subscribe(params => {
      this.invoice_id = params['invoice'];
      console.log(this.invoice_id); // Print the parameter to the console.
      this.fetch_invoice()
    });
  }

  ngOnInit(): void {
  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Product List // Products [ customer ]
  Invoice: any;
  fetch_invoice() {
    this.shopService.invoice_detail({ order_id: this.invoice_id }).subscribe(
      resp => {
        this.Invoice = resp[0]
        console.log(this.Invoice);
      },
      error => {
        console.log(error.statusText);
        if (error.statusText == 'Unauthorized') {
          localStorage.removeItem('token')
          Storage.remove({ key: 'token' });
          // window.location.reload()
          window.location.href = '/signin'
        }
      }
    )
  }
  // End of List Factor
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Whatsapp
  share_on_whatsapp() {
    let url = "https://api.whatsapp.com/send/?phone=91" + this.Invoice[0]?.customer_mobile + "&text=" + window.location.href
    window.open(url, "_blank");
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Printing Bill
  print_bill() {
    console.log("asd");

    window.print()
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


}
