import { MatAccordion } from '@angular/material/expansion';
import { NgForm } from '@angular/forms';
import { ShopService } from './../../backend/shop.service';
import { NavigationExtras, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { Component, OnInit, ViewChild } from '@angular/core';

declare var $: any;
import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;

@Component({
  selector: 'app-expense-management',
  templateUrl: './expense-management.component.html',
  styleUrls: ['./expense-management.component.scss'],
})
export class ExpenseManagementComponent implements OnInit {
  @ViewChild(MatAccordion) accordion: MatAccordion;

  constructor(private router: Router, private shopService: ShopService) {}
  ngOnInit(): void {
    /////////////////////////////////////////////////////////////////////////////////////////////
    this.list_factor();
    this.Factor.expense_payment = 2;
    /////////////////////////////////////////////////////////////////////////////////////////////
  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Variables
  EXPENSE_PAYMENT: any = [
    { id: 1, value: 'Online' },
    { id: 2, value: 'Cash' },
    { id: 3, value: 'Credit' },
  ];
  ACCOUNT_TYPE: any = [
    { id: 1, value: 'Account 1' },
    { id: 2, value: 'Account 2' },
  ];
  Types = [];
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Expense Add
  Factor: any = new Object();
  create_factor(form: NgForm) {
    console.log(this.Factor);
    console.log(form.value);

    this.Factor.expense_payment = form.value.expense_payment;
    this.shopService.expense_create(this.Factor).subscribe((resp) => {
      console.log(resp);
      this.list_factor();
      this.Factor = new Object();
      form.reset();
      this.Factor.expense_payment = 2;
      $('#create-factor-modal').modal('hide');
    });
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching expense List // expense
  message: any = new Object();
  Factors: any = [];
  list_factor() {
    this.Factor.filter_type = 1;
    this.shopService.expense_list(this.Factor).subscribe((resp) => {
      this.Factors = resp;
      console.log(resp);
      this.rerender();
      $('.preloader').fadeOut();
    });
    this.shopService.expense_type_list(this.Factor).subscribe((resp) => {
      this.Types = resp;
      console.log(resp);
      // this.rerender()
      // $(".preloader").fadeOut();
      this.Factor.account_type = 1;
      this.Factor.purchase_payment = 2;
    });
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // To Edit/Delete expense
  edit_factor(form: NgForm) {
    console.log(this.Factor);
    this.shopService.expense_edit(this.Factor).subscribe((resp) => {
      console.log('this.Factor');
      console.log(resp);
      this.list_factor();
      this.Factor = new Object();
      form.reset();
      this.Factor.expense_payment = 2;
      $('#edit-factor-modal').modal('hide');
    });
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  delete_factor(element: any) {
    this.Factor.expense_id = element.expense_id;
    this.shopService.expense_delete(this.Factor).subscribe((resp) => {
      this.list_factor();
    });
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Misc Functions
  show_edit(ele: any) {
    console.log(ele);
    // this.selectedFrom = ele.customer_from
    this.Factor = ele;
    console.log(ele);
    $('#edit-factor-modal').modal('show');
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Create Status [ task ]
  ExpenseType: any = new Object();
  add_expense_type(form: NgForm) {
    this.shopService.expense_type_create(form.value).subscribe((resp) => {
      console.log(resp);
      this.list_factor();
      $('#create-expense-modal').modal('hide');
      this.ExpenseType = new Object();
      form.reset();
    });
  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Delete Factor [ customer -  ]
  delete_expense_type(ele: any) {
    this.ExpenseType.expense_type_id = ele.expense_type_id;
    this.shopService.expense_type_delete(this.ExpenseType).subscribe((resp) => {
      this.list_factor();
      $('#create-expense-modal').modal('hide');
    });
  }

  // End Create Status
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  isExpand: any = false;
  @ViewChild(DataTableDirective, { static: false })
  dtEle: DataTableDirective;
  dtTrigger: Subject<any> = new Subject<any>();
  dtOptions: DataTables.Settings = {
    // responsive: true,
    // ordering: false
  };

  // DataTables Functions
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtEle.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }
  togglePanels() {
    if (this.isExpand) {
      this.accordion.closeAll();
    } else {
      this.accordion.openAll();
    }
    this.isExpand = !this.isExpand;
  }

  isTable = true;
  toggleView(view) {
    var text = view.target.textContent;
    if (text == 'Table View') {
      console.log(view.target.textContent);
      this.config['isTable'].product_database = true;
      Storage.set({ key: 'config', value: JSON.stringify(this.config) });
      this.list_factor();
    } else if (text == 'Panel View') {
      this.config['isTable'].product_database = false;
      Storage.set({ key: 'config', value: JSON.stringify(this.config) });
    }
  }

  config = this.shopService.config;
  update_config() {
    Storage.set({ key: 'config', value: JSON.stringify(this.config) });
  }

  /////////////////////////////////////////////////////////////////////////////////////////////
}
