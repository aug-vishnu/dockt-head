import { NgForm } from '@angular/forms';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { ShopService } from './../../backend/shop.service';
import { Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
declare var $: any;
import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;

@Component({
  selector: 'app-credit-dashboard',
  templateUrl: './credit-dashboard.component.html',
  styleUrls: ['./credit-dashboard.component.scss']
})
export class CreditDashboardComponent implements OnInit {

  constructor(private router: Router, private shopService: ShopService) { }

  ngOnInit(): void {
    /////////////////////////////////////////////////////////////////////////////////////////////
    this.list_factor()
    /////////////////////////////////////////////////////////////////////////////////////////////
  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Variabels
  Schemes: any = []
  Plans: any = []
  Customers: any = []
  Instances: any = []
  Scheme: any = new Object()
  Plan: any = new Object()
  Instance: any = new Object()
  SCHEME_TYPE: any = [{ id: 1, value: 'Daily' }, { id: 2, value: 'Weekly' }, { id: 3, value: 'Monthly' }]

  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching expense List // expense
  list_factor() {
    this.shopService.scheme_list().subscribe(
      resp => {
        this.Schemes = resp
        console.log(resp);
        this.rerender()
        $(".preloader").fadeOut();
      })

    this.shopService.customer_list().subscribe(
      resp => {
        this.Customers = resp
        console.log(resp);
      }
    )

    this.list_plans()
  }

  list_plans() {
    this.shopService.plan_list().subscribe(
      resp => {
        console.log(resp);
        this.Plans = resp
        // console.log(resp);
      })
    this.list_instances()
  }

  list_instances() {
    this.shopService.instance_list().subscribe(
      resp => {
        console.log(resp);
        this.Instances = resp
      })
  }
  /////////////////////////////////////////////////////////////////////////////////////////////



  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Create Category [ customer ]
  create_scheme(form: NgForm) {
    this.shopService.scheme_create(this.Scheme).subscribe(
      resp => {
        console.log(resp)
        this.list_factor()
        $('#create-scheme-modal').modal('hide');
        form.reset()
      },
      error => {
        console.log('error', error);
      }
    );
  }
  // End Create Category
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Update Category [ customer ]
  edit_scheme(form: NgForm) {
    this.shopService.scheme_edit(this.Scheme).subscribe(
      resp => {
        console.log("this.Factor");
        console.log(resp)
        this.list_factor()
        this.Scheme = new Object()
        form.reset()

        $('#edit-scheme-modal').modal('hide');
      },
      error => {
        console.log('error', error);
      }
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Delete Factor [ customer -  ]
  delete_scheme(ele: any) {
    this.Scheme.scheme_id = ele.scheme_id
    this.shopService.scheme_delete(this.Scheme).subscribe(
      resp => {
        this.list_factor()
        $('#edit-scheme-modal').modal('hide');
      },
    )
  }
  // End Delete Factor
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Create Category [ customer ]
  create_scheme_plan(form: NgForm) {
    this.shopService.scheme_plan_create(this.Plan).subscribe(
      resp => {
        console.log(resp)
        this.list_factor()
        $('#create-scheme-plan-modal').modal('hide');
        form.reset()
      },
      error => {
        console.log('error', error);
      }
    );
  }
  // End Create Category
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Update Category [ customer ]
  edit_scheme_plan(form: NgForm) {
    this.shopService.scheme_plan_edit(this.Plan).subscribe(
      resp => {
        console.log("this.Factor");
        console.log(resp)
        this.list_factor()
        this.Plan = new Object()
        form.reset()

        $('#edit-scheme-plan-modal').modal('hide');
      },
      error => {
        console.log('error', error);
      }
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Delete Factor [ customer -  ]
  delete_scheme_plan(ele: any) {
    this.Plan.customer_id = ele.customer_id
    this.shopService.scheme_plan_delete(this.Plan).subscribe(
      resp => {
        this.list_factor()
        $('#edit-scheme-modal').modal('hide');

      },
    )
  }
  // End Delete Factor
  /////////////////////////////////////////////////////////////////////////////////////////////



  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Create Category [ customer ]
  create_plan(form: NgForm) {
    this.shopService.customized_plan_create(this.Scheme).subscribe(
      resp => {
        console.log(resp)
        this.list_factor()
        $('#create-plan-modal').modal('hide');
      },
      error => {
        console.log('error', error);
      }
    );
  }
  // End Create Category
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Update Category [ customer ]
  edit_plan(form: NgForm) {
    this.shopService.customized_plan_edit(this.Scheme).subscribe(
      resp => {
        console.log("this.Factor");
        console.log(resp)
        this.list_factor()
        this.Scheme = new Object()
        $('#edit-plan-modal').modal('hide');
      },
      error => {
        console.log('error', error);
      }
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Delete Factor [ customer -  ]
  delete_plan(ele: any) {
    this.Scheme.customer_id = ele.customer_id
    this.shopService.customized_plan_delete(this.Scheme).subscribe(
      resp => {
        this.list_factor()
        $('#edit-plan-modal').modal('hide');

      },
    )
  }
  // End Delete Factor
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  @ViewChild(DataTableDirective, { static: false })
  dtEle: DataTableDirective;
  dtTrigger: Subject<any> = new Subject<any>();
  dtOptions: DataTables.Settings = {
    // responsive: true,
    pageLength: 100,
    // ordering: false
  };

  // DataTables Functions
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtEle.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }


  isTable = true
  toggleView(view) {
    var text = view.target.textContent
    if (text == "Table View") {
      console.log(view.target.textContent);
      this.config['isTable'].product_database = true
      Storage.set({ key: 'config', value: JSON.stringify(this.config) });
      this.list_factor()
    }
    else if (text == "Panel View") {
      this.config['isTable'].product_database = false
      Storage.set({ key: 'config', value: JSON.stringify(this.config) });
    }
  }

  config = this.shopService.config
  update_config() {
    Storage.set({ key: 'config', value: JSON.stringify(this.config) });
  }

  /////////////////////////////////////////////////////////////////////////////////////////////



}
