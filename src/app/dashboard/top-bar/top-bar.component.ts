import { ShopService } from './../../backend/shop.service';
import { Component, OnInit } from '@angular/core';
declare var $: any;
import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;
@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss']
})
export class TopBarComponent implements OnInit {

  constructor(private shopService: ShopService) { }
  ngOnInit(): void {
    // this.detail_shop()
    this.Shop = this.shopService.Shop_detail
    console.log("top", this.Shop);

  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Shop Detail // Shop
  Shop: any;
  // detail_shop() {
  //   this.shopService.shop_detail().subscribe(
  //     resp => {
  //     })
  // }
  /////////////////////////////////////////////////////////////////////////////////////////////

  logOut() {
    try {
      localStorage.removeItem('token')
    } catch (error) {
      Storage.remove({ key: 'token' });
    }

    window.location.reload()
  }
}
