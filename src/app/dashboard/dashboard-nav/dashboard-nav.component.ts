import { ShopService } from './../../backend/shop.service';
import { CommonService } from './../../backend/common.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;
@Component({
  selector: 'app-dashboard-nav',
  templateUrl: './dashboard-nav.component.html',
  styleUrls: ['./dashboard-nav.component.scss']
})
export class DashboardNavComponent implements OnInit {

  constructor(private router: Router, private shopService: ShopService, private commonService: CommonService) { }

  ngOnInit() {
    this.detail_shop()
  }

  isLoaded: any = false
  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Shop Detail // Shop
  Shop: any;
  detail_shop() {
    this.shopService.shop_detail().subscribe(
      resp => {
        this.Shop = resp
        console.log(resp);
        Storage.set({ key: 'site_id', value: resp['site_id'] });
        this.commonService.get_shop_id()
        this.shopService.get_shop_id()
        this.shopService.set_shop_detail(resp)

        this.shopService.Shop_detail = resp
        console.log("Inside Service  ==>  " + this.shopService.Shop_detail.shop_name);

      })
  }
  /////////////////////////////////////////////////////////////////////////////////////////////
  prviousNav = 1
  toValhalla(nav) {
    var element = $('ul#sidebarnav a')
    element.parentsUntil(".sidebar-nav").each(function (index) {
      if ($(this).is("li") && $(this).children("a").length !== 0) {
        $(this).children("a").addClass("active");
        $(this).parent("ul#sidebarnav").length === 0 ?
          $(this).removeClass("active") :
          $(this).removeClass("selected");
      } else if (!$(this).is("ul") && $(this).children("a").length === 0) {
        $(this).removeClass("selected");

      } else if ($(this).is("ul")) {
        $(this).removeClass('in');
      }
    });
    $("#main-wrapper").toggleClass("show-sidebar");
    $(".nav-toggler i").toggleClass("ti-menu");

    $(".nav" + nav).addClass("selected");
    $(".nav" + this.prviousNav).removeClass("selected");
    if (this.prviousNav == nav) {
      this.prviousNav = nav
    }    // this.router.navigate([route])
  }


  /////////////////////////////////////////////////////////////////////////////////////////////
  // localStorage.setItem('user_type',this.Details.user_type)

  logOut() {
    localStorage.removeItem('token')
    Storage.remove({ key: 'token' });
    window.location.reload()
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

}
