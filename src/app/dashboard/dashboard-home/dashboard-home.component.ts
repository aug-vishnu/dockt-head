import { NgForm } from '@angular/forms';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { ShopService } from './../../backend/shop.service';
import { CommonService } from './../../backend/common.service';
import { Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatAccordion } from '@angular/material/expansion';
declare var $: any;
import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;
@Component({
  selector: 'app-dashboard-home',
  templateUrl: './dashboard-home.component.html',
  styleUrls: ['./dashboard-home.component.scss'],
})
export class DashboardHomeComponent implements OnInit {
  @ViewChild(MatAccordion) accordion: MatAccordion;

  constructor(private router: Router, private shopService: ShopService) {}
  ngOnInit() {
    /////////////////////////////////////////////////////////////////////////////////////////////
    this.detail_shop();
    this.list_factor();
    /////////////////////////////////////////////////////////////////////////////////////////////
    this.Item.order_status = 1;
    this.Item.account_type = 1;
    this.Item.order_payment = 2;
    /////////////////////////////////////////////////////////////////////////////////////////////
  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Date Operations
  Shop: any = new Object();
  Item: any = new Object();
  Customer: any = new Object();

  Items: any = [];
  Products: any = [];
  Customers: any = [];
  Industries: any = [];

  sub_total = 0;

  ORDER_STATUS: any = [
    { id: 1, value: 'Full Payment' },
    { id: 2, value: 'Partial Payment' },
  ];
  ORDER_PAYMENT: any = [
    { id: 1, value: 'Online' },
    { id: 2, value: 'Cash' },
    { id: 3, value: 'Credit' },
  ];
  ACCOUNT_TYPE: any = [
    { id: 1, value: 'Account 1' },
    { id: 2, value: 'Account 2' },
  ];

  CUSTOMER_TYPE: any = [
    { id: 1, value: 'Good' },
    { id: 2, value: 'Medium' },
    { id: 3, value: 'Bad' },
  ];
  CUSTOMER_FROM: any = [
    { id: 1, value: 'Raw Marketing' },
    { id: 2, value: 'Reference' },
    { id: 3, value: 'Regular Customer' },
  ];
  selectedType: any;
  selectedFrom: any;

  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Shop Detail // Shop
  detail_shop() {
    this.Shop = this.shopService.Shop_detail;
    console.log(
      'Inside Service from home' + this.shopService.Shop_detail.shop_name
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching expense List // expense
  list_factor() {
    this.sub_total = 0;
    this.Item.filter_type = 1;
    this.shopService.basket_list(this.Item).subscribe((resp) => {
      this.Items = resp;
      // console.log("Items", resp);
      for (var i = 0; i < resp.length; i++) {
        this.sub_total = this.sub_total + resp[i].item_price;
      }
      this.list_customer();
      this.rerender();
      $('.preloader').fadeOut();
    });
  }

  list_customer() {
    this.shopService.customer_list().subscribe((resp) => {
      this.Customers = resp;
      this.Item.customer = this.Customers[0].customer_id;
      // console.log(resp);
    });
    this.list_product();
  }

  list_product() {
    this.shopService.product_list().subscribe((resp) => {
      this.Products = resp;
      // console.log(resp);
      this.Item.order_status = 1;
      this.Item.account_type = 1;
      this.Item.order_payment = 2;
    });
    this.list_industries();
  }

  list_industries() {
    this.shopService.customer_industry_list().subscribe((resp) => {
      this.Industries = resp;
      // console.log(resp);
      $('.preloader').fadeOut();
      window.scrollTo(0, 0);
    });
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Add Order Item / Random Item
  add_item(form: NgForm) {
    this.shopService.order_item_create(this.Item).subscribe((resp) => {
      console.log(resp);
      this.Item = new Object();
      form.reset();
      this.list_factor();
    });
  }

  check_out(form: NgForm) {
    // console.log(form.value);
    // console.log(this.Item);
    this.Item.order_discount == undefined
      ? (this.Item.order_discount = 0)
      : (this.Item.order_discount = form.value.order_discount);

    this.shopService.check_out(this.Item).subscribe((resp) => {
      console.log(resp);
      this.sub_total = 0;
      this.Item = new Object();
      form.reset();
      this.list_factor();
    });
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Basket Views
  add_qty(item: any) {
    console.log(item);
    item.item_quantity += 1;
    this.shopService.order_item_edit(item).subscribe((resp) => {
      console.log(resp);
      this.list_factor();
    });
  }

  sub_qty(item: any) {
    console.log(item);
    item = item;
    item.item_quantity -= 1;
    this.shopService.order_item_edit(item).subscribe((resp) => {
      console.log(resp);
      this.list_factor();
    });
  }

  del_qty(item: any) {
    console.log(item);
    item = item;
    item.item_quantity += 1;
    this.shopService.order_item_delete(item).subscribe((resp) => {
      console.log(resp);
      this.list_factor();
    });
  }

  print_bill(form: NgForm) {
    this.Item.order_discount == undefined
      ? (this.Item.order_discount = 0)
      : (this.Item.order_discount = form.value.order_discount);

    this.shopService.check_out(this.Item).subscribe((resp) => {
      console.log(resp);
      this.sub_total = 0;
      this.Item = new Object();
      form.reset();
      this.router.navigate(['invoice-detail'], {
        queryParams: { invoice: resp.order_id },
      });
    });
  }

  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Create Category [ customer ]
  Factor: any = new Object();
  create_customer(form: NgForm) {
    this.Factor.customer_from = 1;
    this.Factor.customer_type = 1;
    this.Factor.customer_feedback = null;
    this.Factor.company_name = null;
    this.Factor.customer_city = null;
    this.Factor.shop_industry = null;
    this.Factor.calendar_eligible = false;
    this.Factor.customer_direct = false;
    form.value.customer_feedback == undefined
      ? (this.Factor.customer_feedback = ' - ')
      : console.log('Filled customer_feedback');
    form.value.customer_gstn == undefined
      ? (this.Factor.customer_gstn = null)
      : console.log('Filled customer_gstn');

    console.log(this.Factor);
    this.shopService.customer_create(this.Factor).subscribe(
      (resp) => {
        console.log(resp);
        this.list_factor();
        this.Factor = new Object();
        form.reset();
        $('#create-factor-modal').modal('hide');
      },
      (error) => {
        console.log('error', error);
      }
    );
  }
  // End Create Category
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  @ViewChild(DataTableDirective, { static: false })
  dtEle: DataTableDirective;
  dtEle2: DataTableDirective;
  dtTrigger: Subject<any> = new Subject<any>();
  dtTrigger2: Subject<any> = new Subject<any>();
  dtOptions: DataTables.Settings = {
    responsive: true,
    ordering: false,
  };

  // DataTables Functions
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtEle.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }
  // rerender2(): void {
  //   this.dtEle2.dtInstance.then((dtInstance2: DataTables.Api) => {
  //     // Destroy the table first
  //     dtInstance2.destroy();
  //     // Call the dtTrigger to rerender again
  //     this.dtTrigger2.next();
  //   });
  // }
  /////////////////////////////////////////////////////////////////////////////////////////////
}
