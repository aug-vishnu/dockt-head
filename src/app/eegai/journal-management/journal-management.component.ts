import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';

import { NgForm } from '@angular/forms';
import { ShopService } from './../../backend/shop.service';
import { Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatAccordion } from '@angular/material/expansion';
declare var $: any;


@Component({
  selector: 'app-journal-management',
  templateUrl: './journal-management.component.html',
  styleUrls: ['./journal-management.component.scss']
})
export class JournalManagementComponent implements OnInit {

  @ViewChild(MatAccordion) accordion: MatAccordion;

  constructor(private router: Router, private shopService: ShopService) { }

  ngOnInit() {

    /////////////////////////////////////////////////////////////////////////////////////////////
    this.list_factors()
    /////////////////////////////////////////////////////////////////////////////////////////////
  }


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Date Operations
  PRIORITY: any = [{ id: 1, value: 'High' }, { id: 2, value: 'Normal' }, { id: 3, value: 'Low' }]
  CUSTOMER_TYPE: any = [{ id: 1, value: 'Good' }, { id: 2, value: 'Medium' }, { id: 3, value: 'Bad' }]
  CUSTOMER_FROM: any = [{ id: 1, value: 'Raw Marketing' }, { id: 2, value: 'Reference' }, { id: 3, value: 'Regular Customer' }]

  selectedPriority: any;

  max_today = new Date()
  selected_date = new Date();
  isLoading = false
  isFollowUp = false
  isStateEdit = false
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Tasks Detail // Task
  Tasks: any;
  Machines: any;
  Task: any = new Object();

  list_factors() {
    this.shopService.journal_list().subscribe(
      resp => {
        this.Tasks = resp
        console.log(resp);
        $(".preloader").fadeOut();
        this.rerender()
      })
    this.shopService.machine_list().subscribe(
      resp => {
        this.Machines = resp
        console.log(resp);
        this.rerender()
      })

  }
  /////////////////////////////////////////////////////////////////////////////////////////////



  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Create Category [ journal ]
  Creating_Factor: any = new Object()
  create_factor(form: NgForm) {
    this.Creating_Factor.journal_rating = 5
    this.Creating_Factor.follow_up = this.selected_date
    console.log(form.value);
    console.log(this.Creating_Factor);

    this.shopService.journal_create(this.Creating_Factor).subscribe(
      resp => {
        console.log(resp)
        this.list_factors()
        this.Creating_Factor = new Object()
        $('#create-factor-modal').modal('hide');
      },
      error => {
        console.log('error', error);
      }
    );
  }
  // End Create Category
  /////////////////////////////////////////////////////////////////////////////////////////////



  /////////////////////////////////////////////////////////////////////////////////////////////
  // Update Category [ journal ]
  Editing_Factor: any = new Object()
  edit_factor(form: NgForm) {
    console.log(this.Editing_Factor);
    this.shopService.journal_edit(this.Editing_Factor).subscribe(
      resp => {
        console.log(resp)
        this.list_factors()
        this.Editing_Factor = new Object()
        $('#edit-factor-modal').modal('hide');
      },
      error => {
        console.log('error', error);
      }
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Delete Factor [ journal -  ]
  delete_factor(ele: any) {
    this.Editing_Factor.journal_id = ele.journal_id
    this.shopService.journal_delete(this.Editing_Factor).subscribe(
      resp => {
        this.list_factors()
      },
    )
  }
  // End Delete Factor
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Create Status [ task ]
  Machine: any = new Object()
  add_machine(form: NgForm) {

    this.shopService.machine_create(form.value).subscribe(
      resp => {
        console.log(resp)
        this.list_factors()
        $('#create-machine-modal').modal('hide');
        this.Machine = new Object()
      },
    );
  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Delete Factor [ customer -  ]
  delete_machine(ele: any) {
    this.Machine.machine_id = ele.machine_id
    this.shopService.machine_delete(this.Machine).subscribe(
      resp => {
        this.list_factors()
        $('#create-machine-modal').modal('hide');

      },
    )
  }

  // End Create Status
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Misc Functions
  show_edit(ele: any) {
    this.Editing_Factor = ele
    console.log(ele);
    $('#edit-factor-modal').modal('show')
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Tasks Functions
  next_followup(ele) {
    $('#create-factor-modal').modal('show')
    this.isFollowUp = true
    this.Task = ele

  }
  disclose(ele) {
    this.Editing_Factor = new Object()
    this.Editing_Factor.journal_id = ele.journal_id
    this.Editing_Factor.dislosed_state = true
    this.shopService.journal_edit(this.Editing_Factor).subscribe(
      resp => {
        console.log(resp)
        this.list_factors()
        this.Editing_Factor = new Object()
      },
      error => {
        console.log('error', error);
      }
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////



  /////////////////////////////////////////////////////////////////////////////////////////////
  // Timer Functions
  // Input : Raw DateTime // Output : dd-mm-yy
  formatDateforPicker(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('-');
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  isExpand: any = false
  @ViewChild(DataTableDirective, { static: false })
  dtEle: DataTableDirective;
  dtTrigger: Subject<any> = new Subject<any>();
  dtOptions: DataTables.Settings = {
    // responsive: true,
    // ordering: false
  };


  // DataTables Functions
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtEle.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }
  // togglePanels() {
  //   if (this.isExpand) { this.accordion.closeAll() }
  //   else { this.accordion.openAll() }
  //   this.isExpand = !this.isExpand
  // }
  /////////////////////////////////////////////////////////////////////////////////////////////

}
