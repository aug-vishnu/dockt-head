import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { NgForm } from '@angular/forms';
import { ShopService } from './../../backend/shop.service';
import { Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatAccordion } from '@angular/material/expansion';
declare var $: any;
@Component({
  selector: 'app-invoice-management',
  templateUrl: './invoice-management.component.html',
  styleUrls: ['./invoice-management.component.scss']
})
export class InvoiceManagementComponent implements OnInit {

  @ViewChild(MatAccordion) accordion: MatAccordion;

  constructor(private router: Router, private shopService: ShopService) { }

  ngOnInit() {

    /////////////////////////////////////////////////////////////////////////////////////////////
    this.detail_shop()
    this.list_invoices()
    this.list_customers()
    /////////////////////////////////////////////////////////////////////////////////////////////
  }


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Date Operations
  CUSTOMER_TYPE: any = [{ id: 1, value: 'Good' }, { id: 2, value: 'Medium' }, { id: 3, value: 'Bad' }]
  CUSTOMER_FROM: any = [{ id: 1, value: 'Raw Marketing' }, { id: 2, value: 'Reference' }, { id: 3, value: 'Regular Customer' }]
  RETURN_TYPE: any = [{ id: 1, value: 'Highly Possible ' }, { id: 2, value: 'Normal Possibility' }, { id: 3, value: 'Low Possibility' }]

  selectedPriority: any;

  active_day = new Date()
  dispatch_day = new Date()
  selected_date = new Date();
  isLoading = false
  isFollowUp = false
  isExpand = false
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Shop Detail // Shop
  Shop: any;
  detail_shop() {
    this.shopService.shop_detail().subscribe(
      resp => {
        this.Shop = resp
        console.log(resp);
      })
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Customers Detail // Customers
  Customers: any;
  selectedCustomer: any;
  list_customers() {
    this.shopService.customer_list().subscribe(
      resp => {
        this.Customers = resp
        console.log(resp);
        $(".preloader").fadeOut();
      })
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Factors Detail // Factors
  Factors: any;
  Invoice: any = new Object();
  list_invoices() {
    this.Invoice.filter_type = '1'
    this.Invoice.filter_date_one = this.DateTimetoDay(this.active_day)
    this.shopService.invoice_list().subscribe(
      resp => {
        this.Factors = resp
        console.log(resp);
        this.rerender()
      })
  }
  /////////////////////////////////////////////////////////////////////////////////////////////



  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Create Category [ invoice ]
  Creating_Factor: any = new Object()
  create_factor(form: NgForm) {

    this.Creating_Factor.customer = this.selectedCustomer
    this.Creating_Factor.return_types = form.value.return_types
    this.Creating_Factor.invoice_refernce_no = form.value.invoice_refernce_no
    this.Creating_Factor.invoice_balance = form.value.invoice_balance
    this.Creating_Factor.return_types = 1
    form.value.follow_up_comment == undefined ? this.Creating_Factor.follow_up_comment = ' - ' : console.log('Filled follow_up_comment');

    this.Creating_Factor.next_follow_up = this.DateTimetoDay(this.selected_date) + 'T00:00'
    this.Creating_Factor.dispatch_date = this.DateTimetoDay(this.dispatch_day) + 'T00:00'
    console.log(this.Creating_Factor);
    console.log(this.selected_date);

    this.shopService.invoice_create(this.Creating_Factor).subscribe(
      resp => {
        console.log(resp)
        this.list_invoices()
        this.Creating_Factor = new Object()
        this.selectedPriority = null
        this.selectedCustomer = null
        if (this.isFollowUp) {
          this.disclose(this.Invoice)
          this.isFollowUp = false
        }
        this.Invoice = new Object();
        $('#create-factor-modal').modal('hide');
      },
      error => {
        console.log('error', error);
      }
    );
  }
  // End Create Category
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Update Category [ invoice ]
  Editing_Factor: any = new Object()
  edit_factor(form: NgForm) {
    this.Editing_Factor.customer = this.selectedCustomer
    console.log(this.Editing_Factor);


    this.Editing_Factor.return_types = this.Editing_Factor.return_types
    this.Editing_Factor.invoice_balance = form.value.invoice_balance
    this.Editing_Factor.return_types = 1
    form.value.follow_up_comment == undefined ? this.Editing_Factor.follow_up_comment = ' - ' : console.log('Filled follow_up_comment');

    this.Editing_Factor.dispatch_date = this.DateTimetoDay(this.Editing_Factor.dispatch_date) + 'T00:00'

    this.Editing_Factor.next_follow_up = this.DateTimetoDay(this.selected_date) + 'T00:00'
    this.Editing_Factor.invoice_refernce_no = this.Editing_Factor.invoice_refernce_no

    this.shopService.invoice_create(this.Editing_Factor).subscribe(
      resp => {
        console.log(resp)
        this.list_invoices()
        this.Creating_Factor = new Object()
        this.selectedPriority = null
        this.selectedCustomer = null
        this.disclose(this.Invoice)
        this.isFollowUp = false
        this.Invoice = new Object();
        $('#edit-factor-modal').modal('hide');
      },
      error => {
        console.log('error', error);
      }
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Delete Factor [ invoice -  ]
  delete_factor(ele: any) {
    this.Editing_Factor.customer_id = ele.customer_id
    this.shopService.invoice_delete(this.Editing_Factor).subscribe(
      resp => {
        this.list_invoices()
      },
    )
  }
  // End Delete Factor
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Misc Functions
  onSelect(e) {
    console.log(e);
    this.selectedCustomer = e.customer_id
    console.log(this.selectedCustomer);
  }
  onDateChange(e) {
    console.log(this.active_day);
    this.active_day = e.target.value
    this.list_invoices()
  }
  onDateChange2(e) {
    console.log(this.dispatch_day);
    this.dispatch_day = e.target.value
  }
  onSelect2(e) {
    console.log(e);
    this.selectedPriority = e.id
    console.log(this.selectedPriority);
  }
  show_edit(ele: any) {
    this.Editing_Factor = ele
    console.log(ele);
    $('#edit-factor-modal').modal('show')
  }
  togglePanels() {
    if (this.isExpand) { this.accordion.closeAll() }
    else { this.accordion.openAll() }
    this.isExpand = !this.isExpand
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Factors Functions
  next_followup(ele) {
    this.selectedCustomer = ele.customer_id
    this.Editing_Factor = ele
    $('#edit-factor-modal').modal('show')
    this.isFollowUp = true
    this.Invoice = ele
  }
  disclose(ele) {
    this.Editing_Factor = new Object()
    this.selectedCustomer = ele.customer_id
    this.Editing_Factor.customer = this.selectedCustomer
    this.Editing_Factor.invoice_id = ele.invoice_id
    this.Editing_Factor.dislosed_state = true
    this.shopService.invoice_edit(this.Editing_Factor).subscribe(
      resp => {
        console.log(resp)
        this.list_invoices()
        this.Editing_Factor = new Object()
      },
      error => {
        console.log('error', error);
      }
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////



  /////////////////////////////////////////////////////////////////////////////////////////////
  // Timer Functions
  // Input : Raw DateTime // Output : dd-mm-yy
  DateTimetoDay(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('-');
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  @ViewChild(DataTableDirective, { static: false })
  dtEle: DataTableDirective;
  dtTrigger: Subject<any> = new Subject<any>();
  dtOptions: DataTables.Settings = {
    responsive: true,
    ordering: false
  };
  // DataTables Functions
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtEle.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }
  /////////////////////////////////////////////////////////////////////////////////////////////
}
