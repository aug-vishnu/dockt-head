import { ShopService } from './../../backend/shop.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { SiteService } from './../../backend/site.service';
import { Subject } from 'rxjs';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
declare var $: any;

import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;
@Component({
  selector: 'app-customer-management',
  templateUrl: './customer-management.component.html',
  styleUrls: ['./customer-management.component.scss']
})
export class CustomerManagementComponent implements OnInit {

  constructor(private shopService: ShopService, private router: Router) { }

  ngOnInit(): void {

    /////////////////////////////////////////////////////////////////////////////////////////////
    this.list_factor()
    this.list_industries()
    /////////////////////////////////////////////////////////////////////////////////////////////

  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Variables
  CUSTOMER_TYPE: any = [{ id: 1, value: 'Good' }, { id: 2, value: 'Medium' }, { id: 3, value: 'Bad' }]
  CUSTOMER_FROM: any = [{ id: 1, value: 'Raw Marketing' }, { id: 2, value: 'Reference' }, { id: 3, value: 'Regular Customer' }]
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Product List // Products [ customer ]
  Factors: any = [];
  list_factor() {
    this.shopService.customer_list().subscribe(
      resp => {
        this.Factors = resp
        console.log(resp);
        this.rerender()
      }
    )
  }

  Industries: any = [];
  list_industries() {
    this.shopService.customer_industry_list().subscribe(
      resp => {
        this.Industries = resp
        console.log(resp);
        $(".preloader").fadeOut();
      }
    )
  }
  // End of List Factor
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Create Category [ customer ]
  Factor: any = new Object()
  create_factor(form: NgForm) {
    this.Factor.customer_from = 1
    this.Factor.customer_type = 1
    this.Factor.customer_feedback = null
    this.Factor.company_name = null
    this.Factor.customer_city = null
    this.Factor.shop_industry = null
    this.Factor.calendar_eligible = false
    this.Factor.customer_direct = false
    form.value.customer_feedback == undefined ? this.Factor.customer_feedback = ' - ' : console.log('Filled customer_feedback');
    form.value.customer_gstn == undefined ? this.Factor.customer_gstn = null : console.log('Filled customer_gstn');

    console.log(this.Factor);
    this.shopService.customer_create(this.Factor).subscribe(
      resp => {
        console.log(resp)
        this.list_factor()
        this.Factor = new Object()
        form.reset()
        $('#create-factor-modal').modal('hide');
      },
      error => {
        console.log('error', error);
      }
    );
  }
  // End Create Category
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Update Category [ customer ]
  edit_factor(form: NgForm) {
    console.log(this.Factor);
    this.shopService.customer_edit(this.Factor).subscribe(
      resp => {
        console.log(resp)
        this.list_factor()
        this.Factor = new Object()
        form.reset()
        $('#edit-factor-modal').modal('hide');
      },
      error => {
        console.log('error', error);
      }
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Delete Factor [ customer -  ]
  delete_factor(ele: any) {
    this.Factor.customer_id = ele.customer_id
    this.shopService.customer_delete(this.Factor).subscribe(
      resp => {
        this.list_factor()
        $('#edit-factor-modal').modal('hide');
        this.Factor = new Object()
      },
    )
  }
  // End Delete Factor
  /////////////////////////////////////////////////////////////////////////////////////////////



  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Create Status [ task ]
  Industry: any = new Object()
  add_industry(form: NgForm) {

    this.shopService.customer_industry_create(form.value).subscribe(
      resp => {
        console.log(resp)
        this.list_industries()
        $('#create-industry-modal').modal('hide');
        this.Industry = new Object()
      },
      error => {
        console.log('error', error);
      }
    );
  }
  // End Create Status
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Delete Factor [ customer -  ]
  delete_industry(ele: any) {
    this.Industry.customer_industry_id = ele.customer_industry_id
    this.shopService.customer_industry_delete(this.Industry).subscribe(
      resp => {
        this.list_factor()
        this.list_industries()
        $('#industry-create-modal').modal('hide');

      },
    )
  }
  // End Delete Factor
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Misc Functions
  show_edit(ele: any) {
    console.log(ele);
    this.Factor = ele
    console.log(ele);
    $('#edit-factor-modal').modal('show')
  }
  isCustomerSelected: any = false
  show_detail(ele: any) {
    console.log(ele);
    this.isCustomerSelected = true
    Storage.set({ key: 'customer_id', value: ele.customer_id });
    $('#customer-detail-modal').modal('show')
    // this.isCustomerSelected = false
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  @ViewChild(DataTableDirective, { static: false })
  dtEle: DataTableDirective;
  dtTrigger: Subject<any> = new Subject<any>();
  dtOptions: DataTables.Settings = {
    // responsive: true,
    pageLength: 100,
    // ordering: false
  };

  // DataTables Functions
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtEle.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  isTable = true
  toggleView(view) {
    var text = view.target.textContent
    if (text == "Table View") {
      console.log(view.target.textContent);
      this.config['isTable'].product_database = true
      Storage.set({ key: 'config', value: JSON.stringify(this.config) });
      this.list_factor()
    }
    else if (text == "Panel View") {
      this.config['isTable'].product_database = false
      Storage.set({ key: 'config', value: JSON.stringify(this.config) });
    }
  }

  config = this.shopService.config
  update_config() {
    Storage.set({ key: 'config', value: JSON.stringify(this.config) });
  }

  /////////////////////////////////////////////////////////////////////////////////////////////

}
