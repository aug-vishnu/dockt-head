import { CommonService } from './../../backend/common.service';
import { ShopService } from './../../backend/shop.service';
import { Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatAccordion } from '@angular/material/expansion';

@Component({
  selector: 'app-customer-detail',
  templateUrl: './customer-detail.component.html',
  styleUrls: ['./customer-detail.component.scss']
})
export class CustomerDetailComponent implements OnInit {
  @ViewChild(MatAccordion) accordion: MatAccordion;

  constructor(private router: Router, private shopService: ShopService, private commonService: CommonService) { }

  customer_id: any
  ngOnInit() {
    this.commonService.get_customer_id().then(() => {
      this.customer_id = this.commonService.share_customer_id()
      this.Customer.customer_id = this.customer_id
      console.log(this.customer_id);
      this.detail_customer()
    })

  }


  CUSTOMER_TYPE: any = [{ id: 1, value: 'Good' }, { id: 2, value: 'Medium' }, { id: 3, value: 'Bad' }]
  CUSTOMER_FROM: any = [{ id: 1, value: 'Raw Marketing' }, { id: 2, value: 'Reference' }, { id: 3, value: 'Regular Customer' }]

  isLoaded: any = false
  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Customer Detail // Customer
  Customer: any = new Object();
  detail_customer() {
    this.shopService.customer_detail(this.Customer).subscribe(
      resp => {
        this.Customer = resp
        console.log(resp);
        this.isLoaded = true
        $(".preloader").fadeOut();
      })
  }
  /////////////////////////////////////////////////////////////////////////////////////////////
  isExpand: any = false
  togglePanels() {
    if (this.isExpand) { this.accordion.closeAll() }
    else { this.accordion.openAll() }
    this.isExpand = !this.isExpand
  }
}
