import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';

import { NgForm } from '@angular/forms';
import { ShopService } from './../../backend/shop.service';
import { Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatAccordion } from '@angular/material/expansion';
declare var $: any;

@Component({
  selector: 'app-task-management',
  templateUrl: './task-management.component.html',
  styleUrls: ['./task-management.component.scss']
})
export class TaskManagementComponent implements OnInit {
  @ViewChild(MatAccordion) accordion: MatAccordion;


  constructor(private router: Router, private shopService: ShopService) { }

  ngOnInit() {

    /////////////////////////////////////////////////////////////////////////////////////////////
    this.detail_shop()
    this.list_tasks()
    this.list_customers()
    /////////////////////////////////////////////////////////////////////////////////////////////
  }


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Date Operations
  PRIORITY: any = [{ id: 1, value: 'High' }, { id: 2, value: 'Normal' }, { id: 3, value: 'Low' }]
  CUSTOMER_TYPE: any = [{ id: 1, value: 'Good' }, { id: 2, value: 'Medium' }, { id: 3, value: 'Bad' }]
  CUSTOMER_FROM: any = [{ id: 1, value: 'Raw Marketing' }, { id: 2, value: 'Reference' }, { id: 3, value: 'Regular Customer' }]

  selectedPriority: any;

  today = new Date()
  max_today = new Date()
  selectedDate = this.formatDateforPicker(this.today);
  maxSelectabledate = this.formatDateforPicker(this.max_today);
  selected_date = new Date();
  isLoading = false
  isFollowUp = false
  isStateEdit = false
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Shop Detail // Shop
  Shop: any;
  detail_shop() {
    this.shopService.shop_detail().subscribe(
      resp => {
        this.Shop = resp
        console.log(resp);
      })
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Customers Detail // Customers
  Customers: any;
  selectedCustomer: any;
  list_customers() {
    this.shopService.customer_list().subscribe(
      resp => {
        this.Customers = resp
        console.log(resp);
        $(".preloader").fadeOut();
      })
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Tasks Detail // Task
  Tasks: any;
  Task: any = new Object();
  list_tasks() {
    this.shopService.task_list().subscribe(
      resp => {
        this.Tasks = resp
        console.log(resp);
        this.rerender()
      })
  }
  /////////////////////////////////////////////////////////////////////////////////////////////



  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Create Category [ task ]
  Creating_Factor: any = new Object()
  create_factor(form: NgForm) {

    this.Creating_Factor.customer = this.selectedCustomer
    this.Creating_Factor.task_priority = 1
    this.Creating_Factor.next_follow_up = this.formatDateforPicker(this.selected_date) + 'T00:00'
    console.log(this.Creating_Factor);
    console.log(this.selected_date);

    this.shopService.task_create(this.Creating_Factor).subscribe(
      resp => {
        console.log(resp)
        this.list_tasks()
        this.Creating_Factor = new Object()
        this.selectedPriority = null
        this.selectedCustomer = null
        if (this.isFollowUp) {
          this.disclose(this.Task)
          this.isFollowUp = false
        }
        $('#create-factor-modal').modal('hide');
      },
      error => {
        console.log('error', error);
      }
    );
  }
  // End Create Category
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Create Status [ task ]
  Task_status: any = new Object()
  add_status(event, factor) {
    console.log(event);

    this.Task_status.task = factor.task_id
    this.Task_status.task_state = 1
    this.Task_status.task_status = event

    this.shopService.task_status_create(this.Task_status).subscribe(
      resp => {
        console.log(resp)
        this.list_tasks()
        this.Task_status = new Object()
      },
      error => {
        console.log('error', error);
      }
    );
  }
  show_status_edit() {
    this.isStateEdit = true
  }
  // End Create Status
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Update Category [ task ]
  Editing_Factor: any = new Object()
  edit_factor(form: NgForm) {
    this.Editing_Factor.customer = this.selectedCustomer
    console.log(this.Editing_Factor);

    this.shopService.task_edit(this.Editing_Factor).subscribe(
      resp => {
        console.log("this.Factor");
        console.log(resp)
        this.list_tasks()
        this.Editing_Factor = new Object()
        $('#edit-factor-modal').modal('hide');
      },
      error => {
        console.log('error', error);
      }
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Delete Factor [ task -  ]
  delete_factor(ele: any) {
    this.Editing_Factor.customer_id = ele.customer_id
    this.shopService.task_delete(this.Editing_Factor).subscribe(
      resp => {
        this.list_tasks()
      },
    )
  }
  // End Delete Factor
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Misc Functions
  onSelect(e) {
    console.log(e);
    this.selectedCustomer = e.customer_id
    console.log(this.selectedCustomer);
  }
  onDateChange(e) {
    console.log(this.today);
    this.today = e.target.value
    this.list_tasks()
  }
  onSelect2(e) {
    console.log(e);
    this.selectedPriority = e.id
    console.log(this.selectedPriority);
  }
  show_edit(ele: any) {
    this.Editing_Factor = ele
    console.log(ele);
    $('#edit-factor-modal').modal('show')
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Tasks Functions
  next_followup(ele) {
    this.selectedCustomer = ele.customer_id
    $('#create-factor-modal').modal('show')
    this.isFollowUp = true
    this.Task = ele

  }
  disclose(ele) {
    this.Editing_Factor = new Object()
    this.selectedCustomer = ele.customer_id
    this.Editing_Factor.customer = this.selectedCustomer
    this.Editing_Factor.task_id = ele.task_id
    this.Editing_Factor.dislosed_state = true
    this.shopService.task_edit(this.Editing_Factor).subscribe(
      resp => {
        console.log(resp)
        this.list_tasks()
        this.Editing_Factor = new Object()
      },
      error => {
        console.log('error', error);
      }
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////



  /////////////////////////////////////////////////////////////////////////////////////////////
  // Timer Functions
  // Input : Raw DateTime // Output : dd-mm-yy
  formatDateforPicker(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('-');
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  isExpand: any = false
  @ViewChild(DataTableDirective, { static: false })
  dtEle: DataTableDirective;
  dtTrigger: Subject<any> = new Subject<any>();
  dtOptions: DataTables.Settings = {
    // responsive: true,
    // ordering: false
  };


  // DataTables Functions
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtEle.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }
  // togglePanels() {
  //   if (this.isExpand) { this.accordion.closeAll() }
  //   else { this.accordion.openAll() }
  //   this.isExpand = !this.isExpand
  // }
  /////////////////////////////////////////////////////////////////////////////////////////////

}
